<?php
namespace Tests\Unit;

use App\Http\Controllers\Api\TodoController;
use App\Http\Requests\TodoRequest;
use App\Http\Resources\TodoResource;
use App\Models\Todo;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\JsonResponse;
use Tests\TestCase;

// Import Response class
class TodoApiTest extends TestCase
{

    use RefreshDatabase;

    /** @test */
    public function it_can_return_a_collection_of_todos()
    {
        // Setup: Create three Todo instances manually
        $todos = [
            Todo::create([
                'name' => 'Todo 1',
                'description' => 'Description 1',
                'status' => 'todo',
            ]),
            Todo::create([
                'name' => 'Todo 2',
                'description' => 'Description 2',
                'status' => 'done',
            ]),
            Todo::create([
                'name' => 'Todo 3',
                'description' => 'Description 3',
                'status' => 'todo',
            ]),
        ];

        $controller = new TodoController();

        // Action: Call the index method
        $response = $controller->index();

        $this->assertCount(3, $response);
    }

    /** @test */
    public function it_can_store_a_new_todo()
    {
        // Setup
        $data = [
            'name' => 'Test Todo',
            'description' => 'This is a test todo.',
            'status' => 'to do'
        ];
        $request = new TodoRequest($data);
        $controller = new TodoController();

        // Action
        $response = $controller->store($request);

        // Assertion
        $this->assertInstanceOf(TodoResource::class, $response);;
        $this->assertDatabaseHas('todos', $data);
    }

    /** @test */
    public function it_can_show_a_todo()
    {
        // Setup
        $todo = Todo::create([
            'name' => 'Test Todo',
            'description' => 'This is a test todo.',
            'status' => 'to do',
        ]);
        $controller = new TodoController();

        // Action
        $response = $controller->show($todo);

//         Assertion
        $this->assertInstanceOf(TodoResource::class, $response);
    }

    /** @test */
    public function it_can_update_a_todo()
    {
        // Setup
        $todo = Todo::create([
            'name' => 'Test Todo',
            'description' => 'This is a test todo.',
            'status' => 'to do',
        ]);
        $data = [
            'name' => 'Updated Todo',
            'description' => 'This is an updated todo.',
            'status' => 'to do',
        ];
        $request = new TodoRequest($data);
        $controller = new TodoController();

        // Action
        $response = $controller->update($request, $todo);

        // Assertion
        $this->assertInstanceOf(TodoResource::class, $response);
        $this->assertDatabaseHas('todos', $data);
    }

    /** @test */
    public function it_can_delete_a_todo()
    {
        // Setup
        $todo = Todo::create([
            'name' => 'Test Todo',
            'description' => 'This is a test todo.',
            'status' => 'to do',
        ]);
        $controller = new TodoController();

        // Action
        $response = $controller->destroy($todo);

        // Assertion
        $this->assertInstanceOf(JsonResponse::class, $response);
    }

}
