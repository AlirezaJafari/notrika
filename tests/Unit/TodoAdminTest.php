<?php

namespace Tests\Unit;

use App\Http\Controllers\Admin\TodoController;
use App\Http\Requests\TodoRequest;
use App\Models\Todo;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class TodoAdminTest extends TestCase
{

    /** @test */
    public function it_can_store_a_todo()
    {
        $data = [
            'name' => 'Test Todo',
            'description' => 'This is a test todo.',
            'status' => 'todo',
        ];

        $request = new TodoRequest($data);
        $controller = new TodoController();

        $response = $controller->store($request);

        $this->assertDatabaseHas('todos', $data);
        $this->assertEquals(route('todos.index'), $response->headers->get('Location'));
    }

    /** @test */
    public function it_can_update_a_todo()
    {
        $todo = Todo::create([
            'name' => 'Test Todo',
            'description' => 'This is a test todo.',
            'status' => 'todo',
        ]);

        $data = [
            'name' => 'Updated Todo Name',
            'description' => 'Updated todo description.',
            'status' => 'done',
        ];

        $request = new TodoRequest($data);
        $controller = new TodoController();

        $response = $controller->update($request, $todo);

        $this->assertDatabaseHas('todos', $data);
        $this->assertEquals(route('todos.index'), $response->headers->get('Location'));
    }

    /** @test */
    public function it_can_delete_a_todo()
    {
        $todo = Todo::create([
            'name' => 'Test Todo',
            'description' => 'This is a test todo.',
            'status' => 'todo',
        ]);

        $controller = new TodoController();
        $response = $controller->destroy($todo);

        $this->assertDeleted($todo);
        $this->assertEquals(route('todos.index'), $response->headers->get('Location'));
    }
}
