# Todo App

The Todo App is a simple web application built with Laravel that allows users to manage their tasks.

## Features

- **Create:** Users can create new tasks with a name, description, and status.
- **Read:** Users can view a list of all tasks or view details of a specific task.
- **Update:** Users can update the details or status of a task.
- **Delete:** Users can delete tasks they no longer need.

## Installation

1. Clone the repository to your local machine:

    ```bash
    git clone git@gitlab.com:AlirezaJafari/notrika.git
    ```

2. Navigate to the project directory:

    ```bash
    cd notrika
    ```

3. Install PHP dependencies using Composer:

    ```bash
    composer install
    ```

5. Copy the .env.example file to .env and configure your environment variables:

    ```bash
    cp .env.example .env
    ```

6. Generate an application key:

    ```bash
    php artisan key:generate
    ```

7. Run database migrations to create the necessary tables:

    ```bash
    php artisan migrate
    ```

8. (Optional) Seed the database with sample data:

    ```bash
    php artisan db:seed --class=TodoSeeder
    ```

9. Start the development server:

    ```bash
    php artisan serve
    ```

10. Visit [http://localhost:8000](http://localhost:8000) in your web browser to access the application.

## Usage

Register for an account or log in if you already have one.
Create new tasks by clicking on the "Add Task" button and filling out the form.
View your tasks on the homepage and manage them as needed.
Update task details or status by clicking on the task and editing the form.
Delete tasks by clicking on the delete button next to each task.

## Testing

To run the PHPUnit tests:

```bash
php artisan test
