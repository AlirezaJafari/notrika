<?php
/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Todo;
use Faker\Generator as Faker;

// Import the Todo model

// Import the Todo model

// Import the TodoStatusEnum

$factory->define(Todo::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence,
        'description' => $faker->paragraph,
        'status' => 'To Do', // Assign a valid enum value
    ];
});

