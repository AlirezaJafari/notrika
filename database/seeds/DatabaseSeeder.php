<?php

namespace Database\Seeders;

use App\Models\Todo;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        factory(Todo::class, 10)->create(); // Use the factory helper function
        // Other factories...
    }
}
