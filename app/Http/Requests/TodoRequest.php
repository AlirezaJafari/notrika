<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Enums\TodoStatusEnum;

class TodoRequest extends FormRequest
{
    public function authorize()
    {
        return true; // Adjust as needed based on your authorization logic
    }

    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'description' => 'nullable|string',
            'status' => 'nullable|string|'
        ];
    }
}
