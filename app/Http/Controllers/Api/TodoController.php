<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\TodoRequest;
use App\Http\Resources\TodoResource;
use App\Models\Todo;

class TodoController extends Controller
{
    public function index()
    {
        return TodoResource::collection(Todo::all());
    }

    public function store(TodoRequest $request)
    {

        $validatedData = $request->all();

        $todo = Todo::create($validatedData);


        return new TodoResource($todo);
    }

    public function show(Todo $todo)
    {
        return new TodoResource($todo);
    }

    public function update(TodoRequest $request, Todo $todo)
    {
        $validatedData = $request->all();
        $todo->update($validatedData);

        return new TodoResource($todo);
    }

    public function destroy(Todo $todo)
    {
        $todo->delete();

        return response()->json([], 204);
    }
}
