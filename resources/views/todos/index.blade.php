<!-- resources/views/todos/index.blade.php -->

@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Todos</h1>
        <table class="table">
            <thead>
            <tr>
                <th>Name</th>
                <th>Description</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($todos as $todo)
                <tr>
                    <td>{{ $todo->name }}</td>
                    <td>{{ $todo->description }}</td>
                    <td>{{ $todo->status }}</td>
                    <td>
                        <a href="{{ route('todos.show', $todo->id) }}" class="btn btn-primary">View</a>
                        <a href="{{ route('todos.edit', $todo->id) }}" class="btn btn-secondary">Edit</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
