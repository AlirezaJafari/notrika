<!-- resources/views/todos/show.blade.php -->

@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Todo Details</h1>
        <ul>
            <li><strong>Name:</strong> {{ $todo->name }}</li>
            <li><strong>Description:</strong> {{ $todo->description }}</li>
            <li><strong>Status:</strong> {{ $todo->status }}</li>
        </ul>
        <a href="{{ route('todos.edit', $todo->id) }}" class="btn btn-primary">Edit</a>
        <form action="{{ route('todos.destroy', $todo->id) }}" method="POST" style="display: inline;">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-danger">Delete</button>
        </form>
    </div>
@endsection
