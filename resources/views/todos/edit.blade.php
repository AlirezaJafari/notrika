<!-- resources/views/todos/edit.blade.php -->

@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Edit Todo</h1>
        <form method="POST" action="{{ route('todos.update', $todo->id) }}">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" id="name" name="name" value="{{ $todo->name }}">
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <textarea class="form-control" id="description" name="description">{{ $todo->description }}</textarea>
            </div>
            <div class="form-group">
                <label for="status">Status</label>
                <select class="form-control" id="status" name="status">
                    <option value="todo" {{ $todo->status == 'todo' ? 'selected' : '' }}>To Do</option>
                    <option value="in_progress" {{ $todo->status == 'in_progress' ? 'selected' : '' }}>In Progress</option>
                    <option value="done" {{ $todo->status == 'done' ? 'selected' : '' }}>Done</option>
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Update Todo</button>
        </form>
    </div>
@endsection

